import React, {Component} from 'react';

class HelloWorld extends Component {
	render() {
		return (
			<div className="">
				<p>Hello World</p>
			</div>
		);
	}
}

export default HelloWorld;